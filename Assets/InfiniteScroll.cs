using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InfiniteScroll : MonoBehaviour
{
    [SerializeField] private Vector2 scrollSpeed;
    [SerializeField] protected Transform[] parts;
    [SerializeField] private float partWidth;
    private int partsLength;
    protected Action <Transform> onScrollPartRecycle;
    
    

    private void Awake()
    {
        partsLength = parts.Length;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Transform part in parts)
        {
            part.Translate(scrollSpeed * Time.deltaTime);

            if(part.position.x < partWidth * partsLength/2 * -1)
            {
                part.Translate(new Vector2 (partWidth * partsLength, 0));

                onScrollPartRecycle?.Invoke(part);
            }
        }
    }
}
